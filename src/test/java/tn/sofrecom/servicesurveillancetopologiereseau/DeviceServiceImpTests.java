package tn.sofrecom.servicesurveillancetopologiereseau;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.device.DeviceMonitorRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.device.DeviceMonitorResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.device.DeviceRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.device.DeviceResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.handlers.device.DeviceServiceBusinessException;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.DeviceMapper;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.VendorMapper;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.VendorMapperImpl;
import tn.sofrecom.servicesurveillancetopologiereseau.model.Device;
import tn.sofrecom.servicesurveillancetopologiereseau.model.NetInterface;
import tn.sofrecom.servicesurveillancetopologiereseau.model.Vendor;
import tn.sofrecom.servicesurveillancetopologiereseau.repository.DeviceRepository;
import tn.sofrecom.servicesurveillancetopologiereseau.repository.NetInterfaceRepository;
import tn.sofrecom.servicesurveillancetopologiereseau.repository.VendorRepository;
import tn.sofrecom.servicesurveillancetopologiereseau.service.device.implement.DeviceServiceImp;
import tn.sofrecom.servicesurveillancetopologiereseau.service.vendor.implement.VendorServiceImp;
import tn.sofrecom.servicesurveillancetopologiereseau.subject.DeviceStateSubjectDevice;
import tn.sofrecom.servicesurveillancetopologiereseau.subject.NetInterfaceStateSubject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
 class DeviceServiceImpTests {
    @Mock
    private  DeviceRepository deviceRepository;
    @Mock
    private  VendorServiceImp vendorService;
    @Mock
    private  NetInterfaceRepository netInterfaceRepository;
    @Mock
    private  DeviceMapper deviceMapper;
    @Mock
    private  VendorMapper vendorMapper;
    @Mock
    private  NetInterfaceStateSubject netInterfaceStateSubject;
    @Mock
    private DeviceStateSubjectDevice deviceStateSubject;
    @Mock
    private VendorRepository vendorRepository;

    @InjectMocks
    private DeviceServiceImp deviceService;

    private DeviceRequestDTO deviceRequestDTO;
    private Device device;
    private Device deviceUpdate;
    private Device deviceInfo;
    private Device savedDevice;
    private DeviceResponseDTO expectedDevicesResponseDTO;
    private List<Device> deviceList;

    private List<DeviceResponseDTO> deviceResponseDTOList;

    private VendorResponseDTO vendorsResponseDTO;
    private DeviceMonitorResponseDTO deviceMonitorResponseDTO ;

    private DeviceMonitorRequestDTO deviceMonitorRequestDTO ;

    private List<NetInterface>  interfaceExisting ;

    private Vendor vendor ;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        vendorRepository = Mockito.mock(VendorRepository.class);
        vendorMapper = Mockito.mock(VendorMapperImpl.class);
        deviceRepository = Mockito.mock(DeviceRepository.class);
        netInterfaceRepository = Mockito.mock(NetInterfaceRepository.class);
        deviceMapper = Mockito.mock(DeviceMapper.class);
        netInterfaceStateSubject = Mockito.mock(NetInterfaceStateSubject.class);
        deviceStateSubject = Mockito.mock(DeviceStateSubjectDevice.class);
        vendorService = new VendorServiceImp(vendorRepository, vendorMapper);
        deviceService = new DeviceServiceImp(deviceRepository,vendorService,netInterfaceRepository,deviceMapper,vendorMapper, netInterfaceStateSubject ,deviceStateSubject);

        deviceRequestDTO = new DeviceRequestDTO();
        deviceRequestDTO.setIdDevice(1L);
        deviceRequestDTO.setDeviceName("switch09");
        deviceRequestDTO.setDeviceType("switch");
        deviceRequestDTO.setPort("30033");
        deviceRequestDTO.setTransport("telnet");
        deviceRequestDTO.setIpAddress("192.168.25.56");
        deviceRequestDTO.setIdSite(1L);

        device = new Device();
        device.setIdDevice(1L);
        device.setDeviceName("switch09");
        device.setDeviceType("switch");
        device.setPort("30033");
        device.setTransport("telnet");
        device.setIpAddress("192.168.25.56");
        device.setIdSite(1L);
        device.setIdVendor(1L);

        savedDevice = new Device();
        savedDevice.setIdDevice(1L);
        savedDevice.setDeviceName("switch09");
        savedDevice.setDeviceType("switch");
        savedDevice.setPort("30033");
        savedDevice.setTransport("telnet");
        savedDevice.setIpAddress("192.168.25.56");
        savedDevice.setIdSite(1L);

        vendorsResponseDTO = new VendorResponseDTO();
        vendorsResponseDTO.setIdVendor(1L);
        vendorsResponseDTO.setVendorName("cisco");
        vendorsResponseDTO.setDriver("ios");

        vendor = new Vendor();
        vendor.setIdVendor(1L);
        vendor.setVendorName("cisco");
        vendor.setDriver("ios");

        interfaceExisting = new ArrayList<>();
        interfaceExisting.add(new NetInterface(1L,"eth0","192.158.24.35","up",1L,device));
        interfaceExisting.add(new NetInterface(2L,"eth2","Down","192.158.24.35",1L,device));

        deviceInfo = new Device();
        deviceInfo.setIdDevice(1L);
        deviceInfo.setDeviceName("switch09");
        deviceInfo.setDeviceType("switch");
        deviceInfo.setPort("30033");
        deviceInfo.setTransport("telnet");
        deviceInfo.setIpAddress("192.168.25.56");
        deviceInfo.setIdSite(1L);
        deviceInfo.setNetInterfaces(interfaceExisting);
        deviceInfo.setMemoryValue(30000);
        deviceInfo.setCpuValue(30);


        deviceUpdate = new Device();
        deviceUpdate.setIdDevice(1L);
        deviceUpdate.setDeviceName("switch09");
        deviceUpdate.setDeviceType("switch");
        deviceUpdate.setPort("30033");
        deviceUpdate.setTransport("telnet");
        deviceUpdate.setIpAddress("192.168.25.56");
        deviceUpdate.setIdSite(1L);
        deviceUpdate.setNetInterfaces(interfaceExisting);
        deviceUpdate.setMemoryValue(30000);
        deviceUpdate.setCpuValue(30);

        expectedDevicesResponseDTO = new DeviceResponseDTO();
        expectedDevicesResponseDTO.setIdDevice(1L);
        expectedDevicesResponseDTO.setDeviceName("switch09");
        expectedDevicesResponseDTO.setDeviceType("switch");
        expectedDevicesResponseDTO.setIdSite(1L);
        expectedDevicesResponseDTO.setVendorName("cisco");

        deviceMonitorResponseDTO = new DeviceMonitorResponseDTO();
        deviceMonitorResponseDTO.setPort("30033");
        deviceMonitorResponseDTO.setTransport("telnet");
        deviceMonitorResponseDTO.setIpAddress("192.168.25.56");
        deviceMonitorResponseDTO.setNetInterfaces(interfaceExisting);
        deviceMonitorResponseDTO.setMemoryValue(30000);
        deviceMonitorResponseDTO.setCpuValue(30);

        deviceMonitorRequestDTO = new DeviceMonitorRequestDTO();
        deviceMonitorRequestDTO.setPort("30033");
        deviceMonitorRequestDTO.setTransport("telnet");
        deviceMonitorRequestDTO.setIpAddress("192.168.25.56");
        deviceMonitorRequestDTO.setNetInterfaces(interfaceExisting);
        deviceMonitorRequestDTO.setMemoryValue(30000);
        deviceMonitorRequestDTO.setCpuValue(30);

        interfaceExisting = new ArrayList<>();
        interfaceExisting.add(new NetInterface(1L,"eth0","192.158.24.35","up",1L,device));
        interfaceExisting.add(new NetInterface(2L,"eth2","192.158.24.35","Down",1L,device));

        deviceList = new ArrayList<>();
        deviceList.add(new Device(1L,"switch09","switch",30,30000,"192.168.25.56","30033","telnet",null,null,1L,1L,interfaceExisting,null));
        deviceList.add(new Device(2L,"switch09","switch",30,30000,"192.168.25.56","30033","telnet",null,null,1L,1L,interfaceExisting,null));



        deviceResponseDTOList = new ArrayList<>();
        deviceResponseDTOList.add(new DeviceResponseDTO(1L, "switch09","switch",1L,"cisco"));
        deviceResponseDTOList.add(new DeviceResponseDTO(2L, "switch09","switch",1L,"cisco"));

    }

    @Test
    void testCreateNewDevice() {
        when(deviceMapper.deviceDtoToDevice(deviceRequestDTO)).thenReturn(device);
        when(vendorRepository.findById(deviceRequestDTO.getIdVendor())).thenReturn(Optional.of(vendor));
        when(vendorMapper.vendorDtoToVendor(vendorsResponseDTO)).thenReturn(vendor);
        when(deviceRepository.saveAndFlush(device)).thenReturn(savedDevice);
        when(deviceMapper.deviceToDeviceResponseDto(savedDevice)).thenReturn(expectedDevicesResponseDTO);


        DeviceResponseDTO actualDeviceResponseDTO = deviceService.createNewDevice(deviceRequestDTO);
        assertNotNull(actualDeviceResponseDTO);
        assertEquals(expectedDevicesResponseDTO.getIdDevice(), actualDeviceResponseDTO.getIdDevice());
        assertEquals(expectedDevicesResponseDTO.getDeviceName(), actualDeviceResponseDTO.getDeviceName());
    }

    @Test
    void testUpdateDevice() {
        when(deviceRepository.findById(deviceRequestDTO.getIdDevice())).thenReturn(Optional.of(device));
        when(deviceMapper.deviceToDeviceResponseDto(device)).thenReturn(expectedDevicesResponseDTO);
        when(deviceMapper.deviceDtoToDevice(expectedDevicesResponseDTO)).thenReturn(device);
        when(vendorRepository.findById(deviceRequestDTO.getIdVendor())).thenReturn(Optional.of(vendor));
        when(vendorMapper.vendorDtoToVendor(vendorsResponseDTO)).thenReturn(vendor);
        when(deviceMapper.deviceDtoToDevice(deviceRequestDTO)).thenReturn(device);
        when(deviceRepository.saveAndFlush(device)).thenReturn(savedDevice);
        when(deviceMapper.deviceToDeviceResponseDto(savedDevice)).thenReturn(expectedDevicesResponseDTO);

        DeviceResponseDTO updateDeviceResponseDTO = deviceService.updateDevice(deviceRequestDTO);
        assertNotNull(updateDeviceResponseDTO);
        assertEquals(expectedDevicesResponseDTO.getIdDevice(), updateDeviceResponseDTO.getIdDevice());
        assertEquals(expectedDevicesResponseDTO.getDeviceName(), updateDeviceResponseDTO.getDeviceName());

    }

    @Test
    void testDeleteDevice() {
        Long idDevice = 1L;
        when(deviceRepository.findById(deviceRequestDTO.getIdDevice())).thenReturn(Optional.of(device));
        when(deviceMapper.deviceToDeviceResponseDto(device)).thenReturn(expectedDevicesResponseDTO);
        when(deviceMapper.deviceDtoToDevice(expectedDevicesResponseDTO)).thenReturn(device);
        when(deviceMapper.deviceToDeviceResponseDto(savedDevice)).thenReturn(expectedDevicesResponseDTO);

        deviceService.deleteDevice(idDevice);
        Mockito.verify(deviceRepository, Mockito.times(1)).deleteById(idDevice);
    }

    @Test
    void getDeviceByIdWhenValidIdShouldReturnDeviceResponseDto() {
        Long idDevice = 1L;

        when(deviceRepository.findById(idDevice)).thenReturn(Optional.of(device));
        when(deviceMapper.deviceDtoToDevice(expectedDevicesResponseDTO)).thenReturn(device);
        when(deviceMapper.deviceToDeviceResponseDto(device)).thenReturn(expectedDevicesResponseDTO);

        DeviceResponseDTO result = deviceService.getDeviceById(idDevice);
        assertEquals(idDevice, result.getIdDevice());
    }

    @Test
    void getDeviceByIdWhenExceptionThrownShouldThrowException() {
        Long idDevice = 1L;

        when(deviceRepository.findById(idDevice)).thenReturn(Optional.of(device));
        when(deviceMapper.deviceDtoToDevice(expectedDevicesResponseDTO)).thenReturn(device);
        when(deviceMapper.deviceToDeviceResponseDto(device)).thenReturn(expectedDevicesResponseDTO);


        when(deviceRepository.findById(anyLong())).thenThrow(new RuntimeException("Exception occurred while fetch device from Database"));
        assertThrows(DeviceServiceBusinessException.class, () -> {
            deviceService.getDeviceById(idDevice);
        });
    }

    @Test
    void testGetDevices() {
        when(deviceRepository.findAll()).thenReturn(deviceList);
        when(deviceMapper.deviceToDeviceResponseDto(any())).thenReturn(deviceResponseDTOList.get(0), deviceResponseDTOList.get(1));

        Collection<DeviceResponseDTO> result = deviceService.getDevices();

        assertNotNull(result);
        assertEquals(deviceList.size(), result.size());

        List<DeviceResponseDTO> resultList = new ArrayList<>(result);
        assertEquals(deviceResponseDTOList.get(0), resultList.get(0));
        assertEquals(deviceResponseDTOList.get(1), resultList.get(1));
    }
    @Test
    void updateInfoDevice(){
        when(deviceRepository.findDeviceByIpAddressAndPort(deviceMonitorRequestDTO.getIpAddress(),deviceMonitorRequestDTO.getPort())).thenReturn(device);
        when(netInterfaceRepository.findByDevice_IdDevice(device.getIdDevice())).thenReturn(interfaceExisting);
        when(deviceMapper.deviceMonitorToDeviceMonitorResponseDto(device)).thenReturn(deviceMonitorResponseDTO);
        DeviceMonitorResponseDTO updateInfoDevice = deviceService.updateInfoDevice(deviceMonitorRequestDTO);
        assertNotNull(updateInfoDevice);


    }
    @Test
    void getInfoDeviceById(){
        String DeviceName = "switch09";

        when(deviceRepository.findByDeviceName(DeviceName)).thenReturn(device);
        when(deviceMapper.deviceMonitorToDeviceMonitorResponseDto(device)).thenReturn(deviceMonitorResponseDTO);

        DeviceMonitorResponseDTO infoDeviceById = deviceService.getInfoDeviceByName(DeviceName);
        assertNotNull(infoDeviceById);

    }
}
