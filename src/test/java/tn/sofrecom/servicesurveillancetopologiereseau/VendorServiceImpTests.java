package tn.sofrecom.servicesurveillancetopologiereseau;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.handlers.vendor.VendorBusinessException;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.VendorMapper;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.VendorMapperImpl;
import tn.sofrecom.servicesurveillancetopologiereseau.model.Vendor;
import tn.sofrecom.servicesurveillancetopologiereseau.repository.VendorRepository;
import tn.sofrecom.servicesurveillancetopologiereseau.service.vendor.implement.VendorServiceImp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
 class VendorServiceImpTests {
    @Mock
    private VendorRepository vendorRepository;

    @Mock
    private VendorMapper vendorMapper;

    @InjectMocks
    private VendorServiceImp vendorService;

    private VendorRequestDTO vendorRequestDTO;
    private Vendor vendor;
    private Vendor savedVendor;
    private VendorResponseDTO expectedVendorsResponseDTO;
    private List<Vendor> vendorList;

    private List<VendorResponseDTO> vendorResponseDTOList;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        vendorRepository = Mockito.mock(VendorRepository.class);
        vendorMapper = Mockito.mock(VendorMapperImpl.class);
        vendorService = new VendorServiceImp(vendorRepository, vendorMapper);

        vendorRequestDTO = new VendorRequestDTO();
        vendorRequestDTO.setIdVendor(1L);
        vendorRequestDTO.setVendorName("ciso");
        vendorRequestDTO.setDriver("ios");


        vendor = new Vendor();
        vendor.setIdVendor(1L);
        vendor.setVendorName("ciso");
        vendor.setDriver("ios");

        savedVendor = new Vendor();
        savedVendor.setIdVendor(1L);
        savedVendor.setVendorName("ciso");
        savedVendor.setDriver("ios");

        expectedVendorsResponseDTO = new VendorResponseDTO();
        expectedVendorsResponseDTO.setIdVendor(1L);
        expectedVendorsResponseDTO.setVendorName("cisco");
        expectedVendorsResponseDTO.setDriver("ios");


        vendorList = new ArrayList<>();
        vendorList.add(new Vendor(1L, "cisco", "ios",null));
        vendorList.add(new Vendor(2L, "othervendor", "otherdriver",null));


        vendorResponseDTOList = new ArrayList<>();
        vendorResponseDTOList.add(new VendorResponseDTO(1L, "cisco", "ios"));
        vendorResponseDTOList.add(new VendorResponseDTO(2L, "othervendor", "otherdriver"));
    }

    @Test
    void testCreateNewVendor() {
        when(vendorMapper.vendorDtoToVendor(vendorRequestDTO)).thenReturn(vendor);
        when(vendorRepository.saveAndFlush(vendor)).thenReturn(savedVendor);
        when(vendorMapper.vendorToVendorResponseDto(savedVendor)).thenReturn(expectedVendorsResponseDTO);

        VendorResponseDTO actualVendorResponseDTO = vendorService.createNewVendor(vendorRequestDTO);
        assertNotNull(actualVendorResponseDTO);
        assertEquals(expectedVendorsResponseDTO.getIdVendor(), actualVendorResponseDTO.getIdVendor());
        assertEquals(expectedVendorsResponseDTO.getVendorName(), actualVendorResponseDTO.getVendorName());
    }

    @Test
    void testUpdateVendor() {

        when(vendorRepository.findById(vendorRequestDTO.getIdVendor())).thenReturn(Optional.of(vendor));
        when(vendorMapper.vendorDtoToVendor(expectedVendorsResponseDTO)).thenReturn(vendor);
        when(vendorMapper.vendorDtoToVendor(vendorRequestDTO)).thenReturn(vendor);
        when(vendorRepository.saveAndFlush(vendor)).thenReturn(savedVendor);
        when(vendorMapper.vendorToVendorResponseDto(savedVendor)).thenReturn(expectedVendorsResponseDTO);

        VendorResponseDTO actualVendorResponseDTO = vendorService.updateVendor(vendorRequestDTO);
        assertNotNull(actualVendorResponseDTO);
        assertEquals(expectedVendorsResponseDTO.getIdVendor(), actualVendorResponseDTO.getIdVendor());
        assertEquals(expectedVendorsResponseDTO.getVendorName(), actualVendorResponseDTO.getVendorName());

    }

    @Test
    void testDeleteVendor() {
        Long idVendor = 1L;
        when(vendorRepository.findById(vendorRequestDTO.getIdVendor())).thenReturn(Optional.of(vendor));
        when(vendorMapper.vendorDtoToVendor(expectedVendorsResponseDTO)).thenReturn(vendor);
        when(vendorMapper.vendorToVendorResponseDto(savedVendor)).thenReturn(expectedVendorsResponseDTO);

        vendorService.deleteVendor(idVendor);
        Mockito.verify(vendorRepository, Mockito.times(1)).deleteById(idVendor);
    }

    @Test
    void getVendorByIdWhenValidIdShouldReturnVendorResponseDto() {
        Long idVendor = 1L;

        when(vendorRepository.findById(idVendor)).thenReturn(Optional.of(vendor));
        when(vendorMapper.vendorDtoToVendor(expectedVendorsResponseDTO)).thenReturn(vendor);
        when(vendorMapper.vendorToVendorResponseDto(vendor)).thenReturn(expectedVendorsResponseDTO);

        VendorResponseDTO result = vendorService.getVendorById(idVendor);
        assertEquals(idVendor, result.getIdVendor());
    }

    @Test
    void getVendorByIdWhenExceptionThrownShouldThrowException() {
        Long idVendor = 1L;

        when(vendorRepository.findById(idVendor)).thenReturn(Optional.of(vendor));
        when(vendorMapper.vendorDtoToVendor(expectedVendorsResponseDTO)).thenReturn(vendor);
        when(vendorMapper.vendorToVendorResponseDto(vendor)).thenReturn(expectedVendorsResponseDTO);

        when(vendorRepository.findById(anyLong())).thenThrow(new RuntimeException("Exception occurred while fetch vendor from Database"));
        assertThrows(VendorBusinessException.class, () -> {
            vendorService.getVendorById(idVendor);
        });
    }

    @Test
    void testGetVendors() {
        when(vendorRepository.findAll()).thenReturn(vendorList);
        when(vendorMapper.vendorToVendorResponseDto(any())).thenReturn(vendorResponseDTOList.get(0), vendorResponseDTOList.get(1));

        Collection<VendorResponseDTO> result = vendorService.getVendors();

        assertNotNull(result);
        assertEquals(vendorList.size(), result.size());

        List<VendorResponseDTO> resultList = new ArrayList<>(result);
        assertEquals(vendorResponseDTOList.get(0), resultList.get(0));
        assertEquals(vendorResponseDTOList.get(1), resultList.get(1));
    }

}
