package tn.sofrecom.servicesurveillancetopologiereseau;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.device.DeviceResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.netinterface.NetInterfaceRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.netinterface.NetInterfaceResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.handlers.netinterface.NetInterfaceServiceBusinessException;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.DeviceMapper;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.NetInterfaceMapper;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.VendorMapper;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.VendorMapperImpl;
import tn.sofrecom.servicesurveillancetopologiereseau.model.Device;
import tn.sofrecom.servicesurveillancetopologiereseau.model.NetInterface;
import tn.sofrecom.servicesurveillancetopologiereseau.repository.DeviceRepository;
import tn.sofrecom.servicesurveillancetopologiereseau.repository.NetInterfaceRepository;
import tn.sofrecom.servicesurveillancetopologiereseau.repository.VendorRepository;
import tn.sofrecom.servicesurveillancetopologiereseau.service.device.implement.DeviceServiceImp;
import tn.sofrecom.servicesurveillancetopologiereseau.service.netinterface.implement.NetInterfaceServiceImp;
import tn.sofrecom.servicesurveillancetopologiereseau.service.vendor.implement.VendorServiceImp;
import tn.sofrecom.servicesurveillancetopologiereseau.subject.DeviceStateSubjectDevice;
import tn.sofrecom.servicesurveillancetopologiereseau.subject.NetInterfaceStateSubject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
class NetInterfaceServiceImpTests {
    @Mock
    private DeviceServiceImp deviceService;
    @Mock
    private NetInterfaceRepository interfaceRepository;
    @Mock
    private NetInterfaceMapper interfaceMapper;
    @Mock
    private DeviceMapper deviceMapper;

    @Mock
    private DeviceRepository deviceRepository;
    @Mock
    private VendorServiceImp vendorService;
    @Mock
    private NetInterfaceRepository netInterfaceRepository;
    @Mock
    private VendorMapper vendorMapper;
    @Mock
    private NetInterfaceStateSubject netInterfaceStateSubject;
    @Mock
    private DeviceStateSubjectDevice deviceStateSubject;
    @Mock
    private VendorRepository vendorRepository;

    @InjectMocks
    private NetInterfaceServiceImp netInterfaceService;
    private Device device;

    private NetInterfaceRequestDTO interfaceRequestDTO;
    private NetInterface netInterface;
    private NetInterface savedInterface;
    private NetInterfaceResponseDTO expectedInterfaceResponseDTO;
    private List<NetInterface> interfaceList;
    private DeviceResponseDTO expectedDevicesResponseDTO;
    private List<NetInterfaceResponseDTO> interfaceResponseDTOList;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        interfaceRepository = Mockito.mock(NetInterfaceRepository.class);
        interfaceMapper = Mockito.mock(NetInterfaceMapper.class);
        vendorRepository = Mockito.mock(VendorRepository.class);
        vendorMapper = Mockito.mock(VendorMapperImpl.class);
        deviceRepository = Mockito.mock(DeviceRepository.class);
        netInterfaceRepository = Mockito.mock(NetInterfaceRepository.class);
        deviceMapper = Mockito.mock(DeviceMapper.class);
        netInterfaceStateSubject = Mockito.mock(NetInterfaceStateSubject.class);
        deviceStateSubject = Mockito.mock(DeviceStateSubjectDevice.class);
        vendorService = new VendorServiceImp(vendorRepository, vendorMapper);
        deviceService = new DeviceServiceImp(deviceRepository, vendorService, netInterfaceRepository, deviceMapper, vendorMapper, netInterfaceStateSubject, deviceStateSubject);
        netInterfaceService = new NetInterfaceServiceImp(deviceService, netInterfaceRepository, interfaceMapper, deviceMapper);


        interfaceRequestDTO = new NetInterfaceRequestDTO();
        interfaceRequestDTO.setIdInterface(1L);
        interfaceRequestDTO.setNameInterface("etho");
        interfaceRequestDTO.setInterfaceState("up");
        interfaceRequestDTO.setIpAddressInterface("192.158.24.35");
        interfaceRequestDTO.setIdDevice(1L);


        netInterface = new NetInterface();
        netInterface.setIdInterface(1L);
        netInterface.setNameInterface("etho");
        netInterface.setInterfaceState("up");
        netInterface.setIpAddressInterface("192.158.24.35");
        netInterface.setIdDevice(1L);

        savedInterface = new NetInterface();
        savedInterface.setIdInterface(1L);
        savedInterface.setNameInterface("etho");
        savedInterface.setInterfaceState("up");
        savedInterface.setIpAddressInterface("192.158.24.35");
        savedInterface.setIdDevice(1L);

        expectedDevicesResponseDTO = new DeviceResponseDTO();
        expectedDevicesResponseDTO.setIdDevice(1L);
        expectedDevicesResponseDTO.setDeviceName("switch09");
        expectedDevicesResponseDTO.setDeviceType("switch");
        expectedDevicesResponseDTO.setIdSite(1L);
        expectedDevicesResponseDTO.setVendorName("cisco");


        expectedInterfaceResponseDTO = new NetInterfaceResponseDTO();
        expectedInterfaceResponseDTO.setIdInterface(1L);
        expectedInterfaceResponseDTO.setNameInterface("etho");
        expectedInterfaceResponseDTO.setInterfaceState("up");
        expectedInterfaceResponseDTO.setIpAddressInterface("192.158.24.35");

        device = new Device();
        device.setIdDevice(1L);
        device.setDeviceName("switch09");
        device.setDeviceType("switch");
        device.setPort("30033");
        device.setTransport("telnet");
        device.setIpAddress("192.168.25.56");
        device.setIdSite(1L);
        device.setIdVendor(1L);

        interfaceList = new ArrayList<>();
        interfaceList.add(new NetInterface(1L, "etho", "192.158.24.35","up", 1L,device));
        interfaceList.add(new NetInterface(2L, "etho","192.158.24.35" ,"up", 1L, device));


        interfaceResponseDTOList = new ArrayList<>();
        interfaceResponseDTOList.add(new NetInterfaceResponseDTO(1L, "etho","192.158.24.35", "up", device));
        interfaceResponseDTOList.add(new NetInterfaceResponseDTO(1L, "etho", "192.158.24.35","up", device));
    }

    @Test
    void testCreateNewInterface() {
        when(interfaceMapper.interfaceDtoToInterface(interfaceRequestDTO)).thenReturn(netInterface);
        when(deviceRepository.findById(interfaceRequestDTO.getIdDevice())).thenReturn(Optional.of(device));
        when(deviceMapper.deviceToDeviceResponseDto(device)).thenReturn(expectedDevicesResponseDTO);
        when(deviceMapper.deviceDtoToDevice(expectedDevicesResponseDTO)).thenReturn(device);
        when(netInterfaceRepository.saveAndFlush(netInterface)).thenReturn(savedInterface);
        when(interfaceMapper.interfaceToInterfaceResponseDto(savedInterface)).thenReturn(expectedInterfaceResponseDTO);

        NetInterfaceResponseDTO actualInterfaceResponseDTO = netInterfaceService.createNewInterface(interfaceRequestDTO);
        assertNotNull(actualInterfaceResponseDTO);
        assertEquals(expectedInterfaceResponseDTO.getIdInterface(), actualInterfaceResponseDTO.getIdInterface());
        assertEquals(expectedInterfaceResponseDTO.getNameInterface(), actualInterfaceResponseDTO.getNameInterface());
    }

    @Test
    void testUpdateInterface() {

//        when(interfaceRepository.findById(interfaceRequestDTO.getIdInterface())).thenReturn(Optional.of(netInterface));
//        when(interfaceMapper.interfaceDtoToInterface(expectedInterfaceResponseDTO)).thenReturn(netInterface);
//        when(interfaceMapper.interfaceDtoToInterface(interfaceRequestDTO)).thenReturn(netInterface);
//        when(deviceRepository.findById(interfaceRequestDTO.getIdDevice())).thenReturn(Optional.of(device));
//        when(deviceMapper.deviceToDeviceResponseDto(device)).thenReturn(expectedDevicesResponseDTO);
//        when(deviceMapper.deviceDtoToDevice(expectedDevicesResponseDTO)).thenReturn(device);
//        when(netInterfaceRepository.saveAndFlush(netInterface)).thenReturn(savedInterface);
//        when(interfaceMapper.interfaceToInterfaceResponseDto(savedInterface)).thenReturn(expectedInterfaceResponseDTO);
//
//
//        NetInterfaceResponseDTO updateInterfaceResponseDTO = netInterfaceService.updateInterface(interfaceRequestDTO);
//        assertNotNull(updateInterfaceResponseDTO);
//        assertEquals(expectedInterfaceResponseDTO.getIdInterface(), updateInterfaceResponseDTO.getIdInterface());
//        assertEquals(expectedInterfaceResponseDTO.getInterfaceState(), updateInterfaceResponseDTO.getInterfaceState());

    }

    @Test
    void testDeleteInterface() {
        Long idInterface = 1L;
        when(netInterfaceRepository.findById(idInterface)).thenReturn(Optional.of(netInterface));
        when(interfaceMapper.interfaceDtoToInterface(expectedInterfaceResponseDTO)).thenReturn(netInterface);
        when(interfaceMapper.interfaceToInterfaceResponseDto(savedInterface)).thenReturn(expectedInterfaceResponseDTO);

        netInterfaceService.deleteInterface(idInterface);
        Mockito.verify(netInterfaceRepository, Mockito.times(1)).deleteById(idInterface);
    }

    @Test
    void getVendorByIdWhenValidIdShouldReturnVendorResponseDto() {
        Long idInterface = 1L;

        when(netInterfaceRepository.findById(idInterface)).thenReturn(Optional.of(netInterface));
        when(interfaceMapper.interfaceDtoToInterface(expectedInterfaceResponseDTO)).thenReturn(netInterface);
        when(interfaceMapper.interfaceToInterfaceResponseDto(savedInterface)).thenReturn(expectedInterfaceResponseDTO);

        NetInterfaceResponseDTO result = netInterfaceService.getInterfaceById(idInterface);
        assertEquals(idInterface, result.getIdInterface());
    }

    @Test
    void getVendorByIdWhenExceptionThrownShouldThrowException() {
        Long idInterface = 1L;

        when(netInterfaceRepository.findById(idInterface)).thenReturn(Optional.of(netInterface));
        when(interfaceMapper.interfaceDtoToInterface(expectedInterfaceResponseDTO)).thenReturn(netInterface);
        when(interfaceMapper.interfaceToInterfaceResponseDto(savedInterface)).thenReturn(expectedInterfaceResponseDTO);

        when(netInterfaceRepository.findById(anyLong())).thenThrow(new RuntimeException("Exception occurred while fetch interface from Database"));
        assertThrows(NetInterfaceServiceBusinessException.class, () -> {
           netInterfaceService.getInterfaceById(idInterface);
        });
    }

    @Test
    void testGetVendors() {
        when(netInterfaceRepository.findAll()).thenReturn(interfaceList);
        when(interfaceMapper.interfaceToInterfaceResponseDto(any())).thenReturn(interfaceResponseDTOList.get(0), interfaceResponseDTOList.get(1));

        Collection<NetInterfaceResponseDTO> result = netInterfaceService.getInterfaces();

        assertNotNull(result);
        assertEquals(interfaceList.size(), result.size());

        List<NetInterfaceResponseDTO> resultList = new ArrayList<>(result);
        assertEquals(interfaceResponseDTOList.get(0), resultList.get(0));
        assertEquals(interfaceResponseDTOList.get(1), resultList.get(1));
    }

}
