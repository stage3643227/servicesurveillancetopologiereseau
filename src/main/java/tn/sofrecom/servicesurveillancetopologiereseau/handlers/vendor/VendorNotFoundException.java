package tn.sofrecom.servicesurveillancetopologiereseau.handlers.vendor;

public class VendorNotFoundException extends RuntimeException  {
    public VendorNotFoundException(String message) {
        super(message);
    }
}
