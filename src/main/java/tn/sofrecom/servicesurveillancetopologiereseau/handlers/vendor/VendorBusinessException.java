package tn.sofrecom.servicesurveillancetopologiereseau.handlers.vendor;

public class VendorBusinessException extends RuntimeException {
    public VendorBusinessException(String message) {
        super(message);
    }
}
