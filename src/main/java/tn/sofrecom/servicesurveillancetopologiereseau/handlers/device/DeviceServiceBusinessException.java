package tn.sofrecom.servicesurveillancetopologiereseau.handlers.device;

public class DeviceServiceBusinessException extends RuntimeException {
    public DeviceServiceBusinessException(String message) {
        super(message);
    }
}
