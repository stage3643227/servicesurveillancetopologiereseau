package tn.sofrecom.servicesurveillancetopologiereseau.handlers.device;

public class DeviceNotFoundException extends RuntimeException{
    public DeviceNotFoundException(String message) {
        super(message);
    }
}
