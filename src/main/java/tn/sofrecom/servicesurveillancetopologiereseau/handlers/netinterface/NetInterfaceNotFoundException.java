package tn.sofrecom.servicesurveillancetopologiereseau.handlers.netinterface;

public class NetInterfaceNotFoundException  extends RuntimeException{
    public NetInterfaceNotFoundException(String message) {
        super(message);
    }
}
