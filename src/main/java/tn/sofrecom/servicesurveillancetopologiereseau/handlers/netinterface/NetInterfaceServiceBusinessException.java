package tn.sofrecom.servicesurveillancetopologiereseau.handlers.netinterface;

public class NetInterfaceServiceBusinessException extends RuntimeException {
    public NetInterfaceServiceBusinessException(String message) {
        super(message);
    }
}
