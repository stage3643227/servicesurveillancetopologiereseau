package tn.sofrecom.servicesurveillancetopologiereseau.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.model.Vendor;

@Mapper(componentModel = "spring")
public interface VendorMapper {
    VendorMapper INSTANCE = Mappers.getMapper(VendorMapper.class);

    Vendor vendorDtoToVendor(VendorRequestDTO vendorRequestDTO);

    VendorResponseDTO vendorToVendorResponseDto(Vendor vendor);

    Vendor vendorDtoToVendor(VendorResponseDTO vendorResponseDTO);
}
