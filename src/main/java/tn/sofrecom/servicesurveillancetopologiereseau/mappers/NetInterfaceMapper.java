package tn.sofrecom.servicesurveillancetopologiereseau.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.netinterface.NetInterfaceRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.netinterface.NetInterfaceResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.model.NetInterface;

@Mapper(componentModel = "spring")
public interface NetInterfaceMapper {
    NetInterfaceMapper INSTANCE = Mappers.getMapper(NetInterfaceMapper.class);

    NetInterface interfaceDtoToInterface(NetInterfaceRequestDTO interfaceRequestDTO);


    NetInterfaceResponseDTO interfaceToInterfaceResponseDto(NetInterface Interface);

    NetInterface interfaceDtoToInterface(NetInterfaceResponseDTO interfaceResponseDTO);
}
