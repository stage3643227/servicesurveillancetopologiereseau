package tn.sofrecom.servicesurveillancetopologiereseau.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.device.*;
import tn.sofrecom.servicesurveillancetopologiereseau.model.Device;

@Mapper(componentModel = "spring")
public interface DeviceMapper {
    DeviceMapper INSTANCE = Mappers.getMapper(DeviceMapper.class);

    Device deviceDtoToDevice(DeviceRequestDTO deviceRequestDTO);
    @Mapping(source = "device.vendor.vendorName", target = "vendorName")
    DeviceResponseDTO deviceToDeviceResponseDto(Device device);

    Device deviceDtoToDevice(DeviceResponseDTO deviceResponseDTO);

    DeviceMonitorResponseDTO deviceMonitorToDeviceMonitorResponseDto(Device device);

    Device deviceMonitorDtoToDevice(DeviceMonitorRequestDTO deviceMonitorRequestDTO);
    @Mapping(source = "device.vendor.driver", target = "driver")
    DeviceResponseRobotL3vpn deviceToDeviceResponseRobotL3vpn(Device device);
}
