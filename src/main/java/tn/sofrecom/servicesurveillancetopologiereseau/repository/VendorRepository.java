package tn.sofrecom.servicesurveillancetopologiereseau.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicesurveillancetopologiereseau.model.Vendor;

@Repository
public interface VendorRepository extends JpaRepository<Vendor,Long> {
}
