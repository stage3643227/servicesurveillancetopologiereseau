package tn.sofrecom.servicesurveillancetopologiereseau.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicesurveillancetopologiereseau.model.NetInterface;

import java.util.List;

@Repository
public interface NetInterfaceRepository extends JpaRepository<NetInterface,Long> {
    List<NetInterface> findByDevice_IdDevice (Long idDevice);
}
