package tn.sofrecom.servicesurveillancetopologiereseau.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import tn.sofrecom.servicesurveillancetopologiereseau.model.Device;

import java.util.List;

@Repository
public interface DeviceRepository extends JpaRepository<Device,Long> {
    Device findDeviceByIpAddressAndPort(String ipAddress, String port);
    List <Device> findDeviceByDeviceNameStartingWith(String prefix);
    Device findByDeviceName(String deviceName);

}
