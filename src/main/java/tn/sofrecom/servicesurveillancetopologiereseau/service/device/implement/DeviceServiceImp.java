package tn.sofrecom.servicesurveillancetopologiereseau.service.device.implement;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.device.*;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.handlers.device.DeviceNotFoundException;
import tn.sofrecom.servicesurveillancetopologiereseau.handlers.device.DeviceServiceBusinessException;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.DeviceMapper;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.VendorMapper;
import tn.sofrecom.servicesurveillancetopologiereseau.model.Device;
import tn.sofrecom.servicesurveillancetopologiereseau.model.NetInterface;
import tn.sofrecom.servicesurveillancetopologiereseau.repository.DeviceRepository;
import tn.sofrecom.servicesurveillancetopologiereseau.repository.NetInterfaceRepository;
import tn.sofrecom.servicesurveillancetopologiereseau.service.device.DeviceService;
import tn.sofrecom.servicesurveillancetopologiereseau.service.vendor.implement.VendorServiceImp;
import tn.sofrecom.servicesurveillancetopologiereseau.subject.DeviceStateSubjectDevice;
import tn.sofrecom.servicesurveillancetopologiereseau.subject.NetInterfaceStateSubject;

import java.util.*;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class DeviceServiceImp implements DeviceService {
    private final DeviceRepository deviceRepository;
    private final VendorServiceImp vendorServiceImp;
    private final NetInterfaceRepository netInterfaceRepository;
    private final DeviceMapper deviceMapper;
    private final VendorMapper vendorMapper;
    private final NetInterfaceStateSubject netInterfaceStateSubject;

    private DeviceStateSubjectDevice deviceStateSubject;

    @Override
    public DeviceResponseDTO createNewDevice(DeviceRequestDTO deviceRequestDTO) {
        DeviceResponseDTO deviceResponseDTO;

        try {
            log.info("DeviceService::createNewDevice execution started.");
            Device device = deviceMapper.deviceDtoToDevice(deviceRequestDTO);
            log.info("DeviceService::createNewDevice Device {}.", device);
            VendorResponseDTO vendorResponseDTO = vendorServiceImp.getVendorById(deviceRequestDTO.getIdVendor());
            device.setVendor(vendorMapper.vendorDtoToVendor(vendorResponseDTO));
            log.debug("DeviceService::createNewDevice request parameters {}", device);
            Device deviceResults = deviceRepository.saveAndFlush(device);
            deviceResponseDTO = deviceMapper.deviceToDeviceResponseDto(deviceResults);
            log.debug("DeviceService::createNewDevice received response from Database {}", deviceResults);

        } catch (Exception ex) {
            log.error("Exception occurred while persisting Device to database , Exception message {}", ex.getMessage());
            throw new DeviceServiceBusinessException("Exception occurred while create a new Device");
        }
        log.info("DeviceService::createNewDevice  execution ended.");
        return deviceResponseDTO;
    }

    @Override
    public DeviceResponseDTO updateDevice(DeviceRequestDTO deviceRequestDTO) {
        DeviceResponseDTO deviceResponseDTO;
        Device device;
        try {
            device = deviceMapper.deviceDtoToDevice(getDeviceById(deviceRequestDTO.getIdDevice()));
            log.info("DeviceService::updateDevice execution started.");
            device.setDeviceName(deviceRequestDTO.getDeviceName());
            device.setDeviceType(deviceRequestDTO.getDeviceType());
            device.setIdSite(deviceRequestDTO.getIdSite());
            VendorResponseDTO vendorResponseDTO = vendorServiceImp.getVendorById(deviceRequestDTO.getIdVendor());
            device.setVendor(vendorMapper.vendorDtoToVendor(vendorResponseDTO));
            device.setIpAddress(deviceRequestDTO.getIpAddress());
            device.setPort(deviceRequestDTO.getPort());
            device.setTransport(deviceRequestDTO.getTransport());
            deviceResponseDTO = deviceMapper.deviceToDeviceResponseDto(deviceRepository.saveAndFlush(device));
        } catch (Exception ex) {
            log.error("Exception occurred while update Device to database , Exception message {}", ex.getMessage());
            throw new DeviceServiceBusinessException("Exception occurred while update a Device");
        }
        return deviceResponseDTO;
    }

    @Override
    public void deleteDevice(Long idDevice) {
        log.debug("Request to delete Device: {}", idDevice);
        if (getDeviceById(idDevice) != null)
            deviceRepository.deleteById(idDevice);
        else {
            throw new DeviceServiceBusinessException("Exception occurred while delete a device");
        }
    }

    @Override
    public Collection<DeviceResponseDTO> getDevices() {
        Collection<DeviceResponseDTO> deviceResponseDTOS;
        try {
            log.info("DeviceService::getDevices execution started.");
            Collection<Device> deviceList = deviceRepository.findAll();

            if (!deviceList.isEmpty()) {
                deviceResponseDTOS = deviceList.stream().sorted(Comparator.comparing(Device::getIdDevice))
                        .map(deviceMapper::deviceToDeviceResponseDto)
                        .collect(Collectors.toList());
            } else {
                deviceResponseDTOS = Collections.emptyList();
            }
            log.debug("DeviceService::getDevices  retrieving devices from database  {}", deviceResponseDTOS);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving devices from database , Exception message {}", ex.getMessage());
            throw new DeviceServiceBusinessException("Exception occurred while fetch all devices from Database");
        }

        log.info("DeviceService::getDevices execution ended.");
        return deviceResponseDTOS;
    }

    @Override
    public DeviceResponseDTO getDeviceById(Long idDevice) {
        DeviceResponseDTO deviceResponseDTO;
        try {
            log.info("DeviceService::getDeviceById execution started.");
            Device device = deviceRepository.findById(idDevice)
                    .orElseThrow(() -> new DeviceNotFoundException("Device not found with id " + idDevice));
            log.info("DeviceService::getDeviceById Device {}",device);
            deviceResponseDTO = deviceMapper.deviceToDeviceResponseDto(device);
            log.debug("DeviceService::getDeviceById retrieving Device  from database for id Device {} {}", idDevice, deviceResponseDTO);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Device {} from database , Exception message {}", idDevice, ex.getMessage());
            throw new DeviceServiceBusinessException("Exception occurred while fetch Device from Database " + idDevice);
        }

        log.info("DeviceService::getDeviceById execution ended.");
        return deviceResponseDTO;
    }
    public DeviceMonitorResponseDTO updateInfoDevice(DeviceMonitorRequestDTO deviceMonitorRequestDTO) {
        DeviceMonitorResponseDTO deviceMonitorResponseDTO ;
        Device device;
       List<NetInterface>  interfaceExisting ;

        try {
            device = deviceRepository.findDeviceByIpAddressAndPort(deviceMonitorRequestDTO.getIpAddress(),deviceMonitorRequestDTO.getPort());
            device.setCpuValue(deviceMonitorRequestDTO.getCpuValue());
            device.setMemoryValue(deviceMonitorRequestDTO.getMemoryValue());
            interfaceExisting = netInterfaceRepository.findByDevice_IdDevice(device.getIdDevice());
            for (NetInterface netInterface : deviceMonitorRequestDTO.getNetInterfaces()) {
                for (NetInterface netInterfaceExisting : interfaceExisting){
                    if(netInterfaceExisting.getNameInterface().equals(netInterface.getNameInterface()))
                    {
                        netInterfaceExisting.setInterfaceState(netInterface.getInterfaceState());
                        netInterfaceStateSubject.notifyObservers(netInterfaceExisting);
                    }

                }
            }
            device.setNetInterfaces(interfaceExisting);
            deviceStateSubject.notifyObservers(device);
            deviceMonitorResponseDTO = deviceMapper.deviceMonitorToDeviceMonitorResponseDto(device);
        }catch (Exception ex) {
            log.error("Exception occurred while update information Device to database , Exception message {}", ex.getMessage());
            throw new DeviceServiceBusinessException("Exception occurred while update a Device");
        }
        return deviceMonitorResponseDTO ;
    }

    public DeviceMonitorResponseDTO getInfoDeviceByName(String DeviceName) {
        DeviceMonitorResponseDTO deviceResponseDTO;
        try {
            log.info("DeviceService::getDeviceById execution started.");
            Device device = deviceRepository.findByDeviceName(DeviceName);
            log.info("DeviceService::getDeviceById Device {}",device);
            deviceResponseDTO = deviceMapper.deviceMonitorToDeviceMonitorResponseDto(device);
            log.debug("DeviceService::getDeviceById retrieving Device  from database for id Device {} {}", DeviceName, deviceResponseDTO);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Device {} from database , Exception message {}", DeviceName, ex.getMessage());
            throw new DeviceServiceBusinessException("Exception occurred while fetch Device from Database " + DeviceName);
        }

        log.info("DeviceService::getDeviceById execution ended.");
        return deviceResponseDTO;
    }

    public Collection<DeviceResponseRobotL3vpn> getDevicesRobotTest(DeviceSiteRequest deviceSiteRequest) {
        Collection<DeviceResponseRobotL3vpn> devices;
        List<Device> deviceList = new ArrayList<>();

        try {
            log.info("DeviceService::getDevicesRobotTestL3VPN started.");
            for (String country : deviceSiteRequest.getCountries()) {
                String prefix = "CE-" + country.toUpperCase();
                List<Device> deviceArray = deviceRepository.findDeviceByDeviceNameStartingWith(prefix);
                for (Device item : deviceArray) {
                    Long idSite = item.getIdSite();
                    if (deviceSiteRequest.getSiteIds().contains(idSite)) {
                        deviceList.add(item);
                    }
                }
            }

            if (!deviceList.isEmpty()) {
                devices = deviceList.stream().sorted(Comparator.comparing(Device::getIdDevice))
                        .map(deviceMapper::deviceToDeviceResponseRobotL3vpn)
                        .collect(Collectors.toList());
            } else {
                devices = Collections.emptyList();
            }
            log.debug("DeviceService::getDevicesRobotTestL3VPN  retrieving devices from database  {}", devices);


        } catch (Exception ex) {
            log.error("Exception occurred while retrieving devices from database , Exception message {}", ex.getMessage());
            throw new DeviceServiceBusinessException("Exception occurred while fetch all devices from Database");
        }

        log.info("DeviceService::getDevicesRobotTestL3VPN execution ended.");
        return devices;


    }
    public Map<String, RouterDTO> getRouters (DeviceSiteRequest deviceSiteRequest) {

        List<Device> deviceArrayList = new ArrayList<>();

        log.info("DeviceService::getRouters started.");
        for (String country : deviceSiteRequest.getCountries()) {
            String prefix = "PE_" + country.toUpperCase();
            List<Device> deviceArr = deviceRepository.findDeviceByDeviceNameStartingWith(prefix);
            for (Device item : deviceArr) {
                Long idSite = item.getIdSite();
                if (deviceSiteRequest.getSiteIds().contains(idSite)) {
                    deviceArrayList.add(item);
                }
            }
        }
            Map<String, RouterDTO> routers = new HashMap<>();

            for (Device router : deviceArrayList) {
                RouterDTO routerDto = new RouterDTO(
                        router.getIpAddress(),
                        router.getVendor().getDriver(),
                        router.getUsername(),
                        router.getPassword(),
                        router.getTransport(),
                        router.getPort()
                );

                routers.put(router.getDeviceName(), routerDto);
            }

        return routers;

    }
    public Map<String, RouterDTO> getRoutersCE (DeviceSiteRequest deviceSiteRequest) {

        List<Device> deviceArrayList = new ArrayList<>();

        log.info("DeviceService::getRoutersCE started.");
        for (String country : deviceSiteRequest.getCountries()) {
            String prefix = "CE-" + country.toUpperCase();
            List<Device> deviceArr = deviceRepository.findDeviceByDeviceNameStartingWith(prefix);
            for (Device item : deviceArr) {
                Long idSite = item.getIdSite();
                if (deviceSiteRequest.getSiteIds().contains(idSite)) {
                    deviceArrayList.add(item);
                }
            }
        }
        log.info("{}",deviceArrayList);

        Map<String, RouterDTO> ceRouter = new HashMap<>();

        for (Device router : deviceArrayList) {
            RouterDTO routerDto = new RouterDTO(
                    router.getIpAddress(),
                    router.getVendor().getDriver(),
                    router.getUsername(),
                    router.getPassword(),
                    router.getTransport(),
                    router.getPort()
            );

            ceRouter.put(router.getDeviceName(), routerDto);
        }

        return ceRouter;

    }
    public Device getDeviceByName(String name) {
        return deviceRepository.findByDeviceName(name);
    }
}
