package tn.sofrecom.servicesurveillancetopologiereseau.service.device;

import tn.sofrecom.servicesurveillancetopologiereseau.dto.device.DeviceRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.device.DeviceResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorResponseDTO;

import java.util.Collection;

public interface DeviceService {
    DeviceResponseDTO createNewDevice(DeviceRequestDTO deviceRequestDTO);

    DeviceResponseDTO updateDevice(DeviceRequestDTO deviceRequestDTO);

    void deleteDevice(Long idDevice);

    Collection<DeviceResponseDTO> getDevices();

    DeviceResponseDTO getDeviceById(Long idDevice);
}
