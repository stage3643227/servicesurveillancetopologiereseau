package tn.sofrecom.servicesurveillancetopologiereseau.service.vendor.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.handlers.vendor.VendorBusinessException;
import tn.sofrecom.servicesurveillancetopologiereseau.handlers.vendor.VendorNotFoundException;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.VendorMapper;
import tn.sofrecom.servicesurveillancetopologiereseau.model.Vendor;
import tn.sofrecom.servicesurveillancetopologiereseau.repository.VendorRepository;
import tn.sofrecom.servicesurveillancetopologiereseau.service.vendor.VendorService;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class VendorServiceImp implements VendorService {
    private final VendorRepository vendorRepository;
    private final VendorMapper vendorMapper;

    @Override
    public VendorResponseDTO createNewVendor(VendorRequestDTO vendorRequestDTO) {
        VendorResponseDTO vendorResponseDTO;
        try {
            log.info("VendorService:createNewVendor execution started.");
            Vendor vendor = vendorMapper.vendorDtoToVendor(vendorRequestDTO);
            log.debug("VendorService:createNewVendor request parameters {}", vendor);

            Vendor vendorResults = vendorRepository.saveAndFlush(vendor);
            vendorResponseDTO = vendorMapper.vendorToVendorResponseDto(vendorResults);
            log.debug("VendorService:createNewVendor received response from Database {}", vendorResults);

        } catch (Exception ex) {
            log.error("Exception occurred while persisting Vendor to database , Exception message {}", ex.getMessage());
            throw new VendorBusinessException("Exception occurred while create a new Vendor");
        }
        log.info("VendorService:createNewVendor execution ended.");
        return vendorResponseDTO;

    }

    @Override
    public VendorResponseDTO updateVendor(VendorRequestDTO vendorRequestDTO) {
        VendorResponseDTO vendorResponseDTO;
        Vendor vendor;
        try {
            vendor = vendorMapper.vendorDtoToVendor(getVendorById(vendorRequestDTO.getIdVendor()));
            log.info("VendorService:updateVendor execution started.");
            vendor.setVendorName(vendorRequestDTO.getVendorName());
            vendor.setDriver(vendorRequestDTO.getDriver());
            vendorResponseDTO = vendorMapper.vendorToVendorResponseDto(vendorRepository.saveAndFlush(vendor));
        } catch (Exception ex) {
            log.error("Exception occurred while persisting vendor to database , Exception message {}", ex.getMessage());
            throw new VendorBusinessException("Exception occurred while update a vendor");
        }
        return vendorResponseDTO;
    }

    @Override
    public void deleteVendor(Long idVendor) {
        log.debug("Request to delete Vendor: {}", idVendor);
        if (getVendorById(idVendor) != null)
            vendorRepository.deleteById(idVendor);
        else {
            throw new VendorBusinessException("Exception occurred while delete a Vendor");
        }


    }

    @Override
    public Collection<VendorResponseDTO> getVendors() {

        Collection<VendorResponseDTO> vendorResponseDTOS = null;
        try {
            log.info("VendorService::getVendors execution started.");
            Collection<Vendor> vendorList = vendorRepository.findAll();

            if (!vendorList.isEmpty()) {
                vendorResponseDTOS = vendorList.stream().sorted(Comparator.comparing(Vendor::getIdVendor)).map(vendorMapper::vendorToVendorResponseDto).collect(Collectors.toList());
            } else {
                vendorResponseDTOS = Collections.emptyList();
            }
            log.debug("VendorService::getVendors retrieving Vendor from database  {}", vendorResponseDTOS);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Vendor from database , Exception message {}", ex.getMessage());
            throw new VendorBusinessException("Exception occurred while fetch all Vendors from Database");
        }

        log.info("VendorService::getVendors  execution ended.");
        return vendorResponseDTOS;
    }

    @Override
    public VendorResponseDTO getVendorById(Long idVendor) {
        VendorResponseDTO vendorResponseDTO;
        try {
            log.info("VendorService::getVendorById execution started.");
            Vendor vendor = vendorRepository.findById(idVendor).orElseThrow(() -> new VendorNotFoundException("Vendor not found with id " + idVendor));
            vendorResponseDTO = vendorMapper.vendorToVendorResponseDto(vendor);
            log.debug("VendorService::getVendorById retrieving Vendor  from database for id {} {}", idVendor, vendorResponseDTO);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Vendor {} from database , Exception message {}", idVendor, ex.getMessage());
            throw new VendorBusinessException("Exception occurred while fetch Vendor from Database " + idVendor);
        }

        log.info("VendorService::getVendorById execution ended.");
        return vendorResponseDTO;
    }
}
