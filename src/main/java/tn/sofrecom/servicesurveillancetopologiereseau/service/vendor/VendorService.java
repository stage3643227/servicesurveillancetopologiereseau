package tn.sofrecom.servicesurveillancetopologiereseau.service.vendor;

import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorResponseDTO;

import java.util.Collection;

public interface VendorService {
    VendorResponseDTO createNewVendor(VendorRequestDTO vendorRequestDTO);

    VendorResponseDTO updateVendor(VendorRequestDTO vendorRequestDTO);

    void deleteVendor(Long idVendor);

    Collection<VendorResponseDTO> getVendors();

    VendorResponseDTO getVendorById(Long idVendor);
}
