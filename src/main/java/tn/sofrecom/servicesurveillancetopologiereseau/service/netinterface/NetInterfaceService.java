package tn.sofrecom.servicesurveillancetopologiereseau.service.netinterface;

import tn.sofrecom.servicesurveillancetopologiereseau.dto.netinterface.NetInterfaceRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.netinterface.NetInterfaceResponseDTO;

import java.util.Collection;

public interface NetInterfaceService {

    NetInterfaceResponseDTO createNewInterface(NetInterfaceRequestDTO netInterfaceRequestDTO);

    NetInterfaceResponseDTO updateInterface(NetInterfaceRequestDTO netInterfaceRequestDTO);

    void deleteInterface(Long idInterface);

    Collection<NetInterfaceResponseDTO> getInterfaces();

    NetInterfaceResponseDTO getInterfaceById(Long idInterface);
}
