package tn.sofrecom.servicesurveillancetopologiereseau.service.netinterface.implement;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.device.DeviceResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.netinterface.NetInterfaceRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.netinterface.NetInterfaceResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.handlers.netinterface.NetInterfaceNotFoundException;
import tn.sofrecom.servicesurveillancetopologiereseau.handlers.netinterface.NetInterfaceServiceBusinessException;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.DeviceMapper;
import tn.sofrecom.servicesurveillancetopologiereseau.mappers.NetInterfaceMapper;
import tn.sofrecom.servicesurveillancetopologiereseau.model.NetInterface;
import tn.sofrecom.servicesurveillancetopologiereseau.repository.NetInterfaceRepository;
import tn.sofrecom.servicesurveillancetopologiereseau.service.device.implement.DeviceServiceImp;
import tn.sofrecom.servicesurveillancetopologiereseau.service.netinterface.NetInterfaceService;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Collectors;


@Service
@AllArgsConstructor
@Slf4j
@Transactional
public class NetInterfaceServiceImp implements NetInterfaceService {
    private final DeviceServiceImp  deviceServiceImp;
    private final NetInterfaceRepository netInterfaceRepository;
    private final NetInterfaceMapper netInterfaceMapper;
    private final DeviceMapper deviceMapper;

    @Override
    public NetInterfaceResponseDTO createNewInterface(NetInterfaceRequestDTO netInterfaceRequestDTO) {
        NetInterfaceResponseDTO netInterfaceResponseDTO;

        try {
            log.info("NetInterfaceService::createNewInterface execution started.");
            NetInterface netInterface = netInterfaceMapper.interfaceDtoToInterface(netInterfaceRequestDTO);
            log.info("NetInterfaceService::createNewInterface Interface {}.", netInterface);
            DeviceResponseDTO deviceResponseDTO = deviceServiceImp.getDeviceById(netInterfaceRequestDTO.getIdDevice());
            netInterface.setDevice(deviceMapper.deviceDtoToDevice(deviceResponseDTO));
            log.debug("NetInterfaceService::createNewInterface request parameters {}", netInterface);
            NetInterface netInterfaceResults = netInterfaceRepository.saveAndFlush(netInterface);
            netInterfaceResponseDTO = netInterfaceMapper.interfaceToInterfaceResponseDto(netInterfaceResults);
            log.debug("NetInterfaceService::createNewInterface received response from Database {}", netInterfaceResults);

        } catch (Exception ex) {
            log.error("Exception occurred while persisting Interface to database , Exception message {}", ex.getMessage());
            throw new NetInterfaceServiceBusinessException("Exception occurred while create a new Interface");
        }
        log.info("NetInterfaceService::createNewInterface  execution ended.");
        return netInterfaceResponseDTO;
    }

    @Override
    public NetInterfaceResponseDTO updateInterface(NetInterfaceRequestDTO netInterfaceRequestDTO) {
        NetInterfaceResponseDTO netInterfaceResponseDTO;
        NetInterface netInterface ;
        try {
            netInterface = netInterfaceMapper.interfaceDtoToInterface(getInterfaceById(netInterfaceRequestDTO.getIdInterface()));
            log.info("NetInterfaceService::updateInterface execution started.");
            netInterface.setNameInterface(netInterfaceRequestDTO.getNameInterface());
            netInterface.setInterfaceState(netInterfaceRequestDTO.getInterfaceState());
            netInterface.setIpAddressInterface(netInterfaceRequestDTO.getIpAddressInterface());
            DeviceResponseDTO deviceResponseDTO = deviceServiceImp.getDeviceById(netInterfaceRequestDTO.getIdDevice());
            netInterface.setDevice(deviceMapper.deviceDtoToDevice(deviceResponseDTO));
            netInterfaceResponseDTO = netInterfaceMapper.interfaceToInterfaceResponseDto(netInterfaceRepository.saveAndFlush(netInterface));
        } catch (Exception ex) {
            log.error("Exception occurred while persisting Interface to database , Exception message {}", ex.getMessage());
            throw new NetInterfaceServiceBusinessException("Exception occurred while update a Interface");
        }
        return netInterfaceResponseDTO;
    }

    @Override
    public void deleteInterface(Long idInterface) {
        log.debug("Request to delete Interface: {}", idInterface);
        if (getInterfaceById(idInterface) != null)
            netInterfaceRepository.deleteById(idInterface);
        else {
            throw new NetInterfaceServiceBusinessException("Exception occurred while delete a Interface");
        }
    }

    @Override
    public Collection<NetInterfaceResponseDTO> getInterfaces() {
        Collection<NetInterfaceResponseDTO> netInterfaceResponseDTO;
        try {
            log.info("NetInterfaceService::getInterfaces execution started.");
            Collection<NetInterface> netInterfaceList = netInterfaceRepository.findAll();

            if (!netInterfaceList.isEmpty()) {
                netInterfaceResponseDTO = netInterfaceList.stream().sorted(Comparator.comparing(NetInterface::getIdInterface))
                        .map(netInterfaceMapper::interfaceToInterfaceResponseDto)
                        .collect(Collectors.toList());
            } else {
                netInterfaceResponseDTO = Collections.emptyList();
            }
            log.debug("NetInterfaceService::getInterfaces retrieving devices from database  {}", netInterfaceResponseDTO);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Interfaces from database , Exception message {}", ex.getMessage());
            throw new NetInterfaceServiceBusinessException("Exception occurred while fetch all Interfaces from Database");
        }

        log.info("NetInterfaceService::getInterfaces execution ended.");
        return netInterfaceResponseDTO;
    }

    @Override
    public NetInterfaceResponseDTO getInterfaceById(Long idInterface) {
        NetInterfaceResponseDTO netInterfaceResponseDTO;
        try {
            log.info("NetInterfaceService::getInterfaceById execution started. {}",idInterface);
            NetInterface netInterface = netInterfaceRepository.findById(idInterface)
                    .orElseThrow(() -> new NetInterfaceNotFoundException("Interface not found with id " + idInterface));
            log.info("NetInterfaceService::getInterfaceById  Device {}",netInterface);
            netInterfaceResponseDTO = netInterfaceMapper.interfaceToInterfaceResponseDto(netInterface);
            log.debug("NetInterfaceService::getInterfaceById  retrieving Interface  from database for id Interface {} {}", idInterface, netInterfaceResponseDTO);

        } catch (Exception ex) {
            log.error("Exception occurred while retrieving Interface {} from database , Exception message {}", idInterface, ex.getMessage());
            throw new NetInterfaceServiceBusinessException("Exception occurred while fetch Interface from Database " + idInterface);
        }

        log.info("NetInterfaceService::getInterfaceById execution ended.");
        return netInterfaceResponseDTO;
    }
}
