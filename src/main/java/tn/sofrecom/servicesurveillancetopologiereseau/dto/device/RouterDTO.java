package tn.sofrecom.servicesurveillancetopologiereseau.dto.device;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RouterDTO {
    private String host;
    private String driverName;
    private String username;
    private String password;
    private String transport;
    private String port;
}
