package tn.sofrecom.servicesurveillancetopologiereseau.dto.device;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceSiteRequest {
    private    List<String> countries ;
    private List<Long> siteIds;
}
