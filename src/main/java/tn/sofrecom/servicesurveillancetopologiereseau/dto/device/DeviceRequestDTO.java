package tn.sofrecom.servicesurveillancetopologiereseau.dto.device;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class DeviceRequestDTO {
    private Long idDevice;
    @NotBlank(message = "Device name shouldn't be NULL OR EMPTY")
    private String deviceName;
    @NotBlank(message = "Device Type shouldn't be NULL OR EMPTY")
    private String deviceType;
    @NotBlank(message = "Device ip address shouldn't be NULL OR EMPTY")
    private String ipAddress;
    @NotBlank(message = "Device port shouldn't be NULL OR EMPTY")
    private String port;

    @NotBlank(message = "Device transport shouldn't be NULL OR EMPTY")
    private String transport;
    @NotNull(message = "Site id shouldn't be NULL")
    private Long idSite;
    @NotNull(message = "Vendor id shouldn't be NULL")
    private Long idVendor;

}
