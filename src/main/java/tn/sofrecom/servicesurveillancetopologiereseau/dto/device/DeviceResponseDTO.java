package tn.sofrecom.servicesurveillancetopologiereseau.dto.device;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class DeviceResponseDTO {
    private Long idDevice;
    private String deviceName;

    private String deviceType;

    private Long idSite;

    private String vendorName;
}
