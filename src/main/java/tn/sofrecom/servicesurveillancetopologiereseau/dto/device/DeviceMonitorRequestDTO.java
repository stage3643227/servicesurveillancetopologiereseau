package tn.sofrecom.servicesurveillancetopologiereseau.dto.device;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tn.sofrecom.servicesurveillancetopologiereseau.model.NetInterface;


import java.util.Collection;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class DeviceMonitorRequestDTO {
    @NotBlank(message = "Device ip address shouldn't be NULL OR EMPTY")
    private String ipAddress;
    @NotBlank(message = "Device port shouldn't be NULL OR EMPTY")
    private String port;
    @NotBlank(message = "Device transport shouldn't be NULL OR EMPTY")
    private String transport;
    @NotNull(message = "Device Used Memory shouldn't be NULL")
    private Integer memoryValue;
    @NotNull(message = "Device CPU Usage shouldn't be NULL")
    private Integer cpuValue;
    private Collection<NetInterface> netInterfaces;
}
