package tn.sofrecom.servicesurveillancetopologiereseau.dto.device;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tn.sofrecom.servicesurveillancetopologiereseau.model.NetInterface;

import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class DeviceResponseRobotL3vpn {

    private String deviceName;
    private String ipAddress;
    private String port;

    private String transport;

    private String driver;
    private Collection<NetInterface> netInterfaces;

}
