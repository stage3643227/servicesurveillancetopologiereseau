package tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class VendorRequestDTO {
    private Long idVendor;
    @NotBlank(message = "Vendor name shouldn't be NULL OR EMPTY")
    private String vendorName;

    private String driver;
}
