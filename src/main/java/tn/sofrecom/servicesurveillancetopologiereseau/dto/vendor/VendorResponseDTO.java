package tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class VendorResponseDTO {
    private Long idVendor;
    private String vendorName;

    private String driver;
}
