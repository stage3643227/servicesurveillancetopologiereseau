package tn.sofrecom.servicesurveillancetopologiereseau.dto.netinterface;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import tn.sofrecom.servicesurveillancetopologiereseau.model.Device;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class NetInterfaceResponseDTO {
    private Long idInterface;

    private String nameInterface;

    private String interfaceState;

    private String ipAddressInterface;
    private Device device;
}
