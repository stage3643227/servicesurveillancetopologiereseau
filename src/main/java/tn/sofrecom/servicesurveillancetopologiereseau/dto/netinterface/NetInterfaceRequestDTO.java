package tn.sofrecom.servicesurveillancetopologiereseau.dto.netinterface;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class NetInterfaceRequestDTO {
    private Long idInterface;
    @NotBlank(message = "Interface name shouldn't be NULL OR EMPTY")
    private String nameInterface;

    @NotBlank(message = "Interface State shouldn't be NULL OR EMPTY")
    private String interfaceState;

    private String ipAddressInterface;
    @NotNull(message = "Device id shouldn't be NULL")
    private Long idDevice;
}
