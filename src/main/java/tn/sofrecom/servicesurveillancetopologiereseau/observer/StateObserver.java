package tn.sofrecom.servicesurveillancetopologiereseau.observer;

import tn.sofrecom.servicesurveillancetopologiereseau.model.Device;
import tn.sofrecom.servicesurveillancetopologiereseau.model.NetInterface;

public interface StateObserver {
    void update(Device device);
    void update(NetInterface netInterface);
}
