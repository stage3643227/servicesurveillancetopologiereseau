package tn.sofrecom.servicesurveillancetopologiereseau.observer;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.device.DeviceResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.handlers.device.DeviceServiceBusinessException;
import tn.sofrecom.servicesurveillancetopologiereseau.model.Device;
import tn.sofrecom.servicesurveillancetopologiereseau.model.NetInterface;
import tn.sofrecom.servicesurveillancetopologiereseau.repository.DeviceRepository;
import tn.sofrecom.servicesurveillancetopologiereseau.repository.NetInterfaceRepository;
import tn.sofrecom.servicesurveillancetopologiereseau.service.device.implement.DeviceServiceImp;

@Slf4j
@Component
public class DatabaseStateObserver implements StateObserver{
    private DeviceRepository deviceRepository;
    private NetInterfaceRepository netInterfaceRepository;

    public DatabaseStateObserver(DeviceRepository deviceRepository , NetInterfaceRepository netInterfaceRepository) {

        this.deviceRepository = deviceRepository;
        this.netInterfaceRepository = netInterfaceRepository ;
    }

    @Override
    public void update(Device device) {
        try
        {
            deviceRepository.saveAndFlush(device);
        }
        catch (Exception ex) {
            log.error("Exception occurred while update Device {} , Exception message {}", device, ex.getMessage());
            throw new DeviceServiceBusinessException("Exception occurred while update Device from Database " + device);
        }
    }

    @Override
    public void update(NetInterface netInterface) {
        try {
            netInterfaceRepository.saveAndFlush(netInterface);
        } catch (Exception ex) {
            log.error("Exception occurred while updating NetInterface {} , Exception message {}", netInterface, ex.getMessage());
            throw new DeviceServiceBusinessException("Exception occurred while updating NetInterface from Database " + netInterface);
        }
    }
}
