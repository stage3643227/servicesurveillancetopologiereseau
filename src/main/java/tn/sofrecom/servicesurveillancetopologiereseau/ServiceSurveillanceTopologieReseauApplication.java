package tn.sofrecom.servicesurveillancetopologiereseau;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tn.sofrecom.servicesurveillancetopologiereseau.observer.DatabaseStateObserver;
import tn.sofrecom.servicesurveillancetopologiereseau.subject.DeviceStateSubjectDevice;

@SpringBootApplication
public class ServiceSurveillanceTopologieReseauApplication {
	@Autowired
	private DatabaseStateObserver databaseStateObserver;
	@Autowired
	private DeviceStateSubjectDevice deviceStateSubject;


	public static void main(String[] args) {
		SpringApplication.run(ServiceSurveillanceTopologieReseauApplication.class, args);
	}
	@Autowired
	public void setupObserver() {
		deviceStateSubject.registerObserver(databaseStateObserver);
	}
}
