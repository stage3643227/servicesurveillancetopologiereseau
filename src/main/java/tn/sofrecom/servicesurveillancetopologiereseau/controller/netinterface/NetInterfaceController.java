package tn.sofrecom.servicesurveillancetopologiereseau.controller.netinterface;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.APIResponse;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.netinterface.NetInterfaceRequestDTO;


public interface NetInterfaceController {
    @PostMapping
    ResponseEntity<APIResponse> createNewInterface(NetInterfaceRequestDTO netInterfaceRequestDTO);
    @PutMapping
    ResponseEntity<APIResponse> updateInterface(NetInterfaceRequestDTO netInterfaceRequestDTO);
    @DeleteMapping("/{idInterface}")
    ResponseEntity<Void> deleteInterface(@PathVariable Long idInterface);
    @GetMapping("/{idInterface}")
    ResponseEntity<APIResponse> getInterfaceById(@PathVariable Long idInterface);
    @GetMapping("/all")
    ResponseEntity<APIResponse> getAllInterfaces();
}

