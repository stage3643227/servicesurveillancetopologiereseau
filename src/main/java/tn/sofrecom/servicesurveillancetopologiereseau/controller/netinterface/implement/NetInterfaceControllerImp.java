package tn.sofrecom.servicesurveillancetopologiereseau.controller.netinterface.implement;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.sofrecom.servicesurveillancetopologiereseau.controller.netinterface.NetInterfaceController;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.APIResponse;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.netinterface.NetInterfaceRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.netinterface.NetInterfaceResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.service.netinterface.implement.NetInterfaceServiceImp;

import java.util.Collection;
import java.util.List;

@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/api/v1/Interface")
@CrossOrigin(origins = "http://localhost:4200")
public class NetInterfaceControllerImp implements NetInterfaceController {
    public static final String SUCCESS = "Success";
    private NetInterfaceServiceImp netInterfaceServiceImp;

    @Override
    public ResponseEntity<APIResponse> createNewInterface(@Valid @RequestBody NetInterfaceRequestDTO netInterfaceRequestDTO) {
        log.info("NetInterfaceController::createNewInterface request body {}", netInterfaceRequestDTO);
        NetInterfaceResponseDTO netInterfaceResponseDTO = netInterfaceServiceImp.createNewInterface(netInterfaceRequestDTO);
        APIResponse<NetInterfaceResponseDTO> responseDTO = APIResponse
                .<NetInterfaceResponseDTO>builder()
                .status(SUCCESS)
                .results(netInterfaceResponseDTO)
                .build();

        log.info("NetInterfaceController::createNewInterface {}", netInterfaceResponseDTO);
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<APIResponse> updateInterface(@Valid @RequestBody NetInterfaceRequestDTO netInterfaceRequestDTO) {
        log.info("NetInterfaceController::updateInterface request body {}", netInterfaceRequestDTO);
        NetInterfaceResponseDTO netInterfaceResponseDTO  = netInterfaceServiceImp.updateInterface(netInterfaceRequestDTO);
        APIResponse<NetInterfaceResponseDTO> responseDTO = APIResponse
                .<NetInterfaceResponseDTO>builder()
                .status(SUCCESS)
                .results(netInterfaceResponseDTO)
                .build();

        log.info("NetInterfaceController::updateDevice response {}", netInterfaceResponseDTO);
        return new ResponseEntity<>(responseDTO, HttpStatus.ACCEPTED);
    }

    @Override
    public ResponseEntity<Void> deleteInterface(Long idInterface) {
        log.info("NetInterfaceController::deleteInterface by idInterface  {}", idInterface);
        netInterfaceServiceImp.deleteInterface(idInterface);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<APIResponse> getInterfaceById(Long idInterface) {
        log.info("NetInterfaceController::getDeviceById by idInterface  {}", idInterface);
        NetInterfaceResponseDTO netInterfaceResponseDTO = netInterfaceServiceImp.getInterfaceById(idInterface);
        APIResponse<NetInterfaceResponseDTO> responseDTO = APIResponse
                .<NetInterfaceResponseDTO>builder()
                .status(SUCCESS)
                .results(netInterfaceResponseDTO)
                .build();

        log.info("NetInterfaceController::getDeviceById by idInterface {} response {}", idInterface, netInterfaceResponseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<APIResponse> getAllInterfaces() {
        Collection<NetInterfaceResponseDTO> netInterfaceResponseDTOS = netInterfaceServiceImp.getInterfaces();
        APIResponse<List<NetInterfaceResponseDTO>> responseDTO = APIResponse
                .<List<NetInterfaceResponseDTO>>builder()
                .status(SUCCESS)
                .results((List<NetInterfaceResponseDTO>) netInterfaceResponseDTOS)
                .build();

        log.info("NetInterfaceController::getAllInterfaces response {}", responseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
}
