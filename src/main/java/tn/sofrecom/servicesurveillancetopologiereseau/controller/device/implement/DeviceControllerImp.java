package tn.sofrecom.servicesurveillancetopologiereseau.controller.device.implement;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicesurveillancetopologiereseau.controller.device.DeviceController;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.APIResponse;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.device.*;
import tn.sofrecom.servicesurveillancetopologiereseau.service.device.implement.DeviceServiceImp;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/api/v1/Device")
@CrossOrigin(origins = "http://localhost:4200")
public class DeviceControllerImp implements DeviceController {
    public static final String SUCCESS = "Success";
    private DeviceServiceImp deviceServiceImp;
    @Override
    public ResponseEntity<APIResponse> createNewDevice(@Valid @RequestBody  DeviceRequestDTO deviceRequestDTO) {
        log.info("DeviceController::createNewDevice request body {}", deviceRequestDTO);
        DeviceResponseDTO deviceResponseDTO = deviceServiceImp.createNewDevice(deviceRequestDTO);
        APIResponse<DeviceResponseDTO> responseDTO =   APIResponse
                .<DeviceResponseDTO>builder()
                .status(SUCCESS)
                .results(deviceResponseDTO)
                .build();

        log.info("DeviceController::createNewDevice {}", deviceResponseDTO );
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<APIResponse> updateDevice(@Valid @RequestBody DeviceRequestDTO deviceRequestDTO) {
        log.info("DeviceController::updateDevice request body {}", deviceRequestDTO);
        DeviceResponseDTO deviceResponseDTO = deviceServiceImp.updateDevice(deviceRequestDTO);
        APIResponse<DeviceResponseDTO> responseDTO = APIResponse
                .<DeviceResponseDTO>builder()
                .status(SUCCESS)
                .results(deviceResponseDTO)
                .build();

        log.info("DeviceController::updateDevice response {}", deviceResponseDTO );
        return new ResponseEntity<>(responseDTO, HttpStatus.ACCEPTED);
    }

    @Override
    public ResponseEntity<Void> deleteDevice(Long idDevice) {
        log.info("DeviceController::deleteDevice by idDevice  {}", idDevice);
        deviceServiceImp.deleteDevice(idDevice);
        return  ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<APIResponse> getDeviceById(Long idDevice) {
        log.info("DeviceController::getDeviceById by idDevice  {}", idDevice);
        DeviceResponseDTO deviceResponseDTO = deviceServiceImp.getDeviceById(idDevice);
        APIResponse<DeviceResponseDTO> responseDTO = APIResponse
                .<DeviceResponseDTO>builder()
                .status(SUCCESS)
                .results(deviceResponseDTO)
                .build();

        log.info("DeviceController::getDeviceById by idDevice {} response {}", idDevice,deviceResponseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<APIResponse> getAllDevices() {
        Collection<DeviceResponseDTO> deviceResponseDTOS = deviceServiceImp.getDevices();
        APIResponse<List<DeviceResponseDTO>> responseDTO = APIResponse
                .<List<DeviceResponseDTO>>builder()
                .status(SUCCESS)
                .results((List<DeviceResponseDTO>) deviceResponseDTOS)
                .build();

        log.info("DeviceController::getAllDevices response {}", deviceResponseDTOS);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
    @PostMapping("/monitor")
    public ResponseEntity<APIResponse> updateInfoDevice(@Valid @RequestBody DeviceMonitorRequestDTO deviceMonitorRequestDTO) {
        log.info("DeviceController::updateInfoDevice request body {}", deviceMonitorRequestDTO);
        DeviceMonitorResponseDTO deviceMonitorResponseDTO = deviceServiceImp.updateInfoDevice(deviceMonitorRequestDTO);
        APIResponse<DeviceMonitorResponseDTO> responseDTO = APIResponse
                .<DeviceMonitorResponseDTO>builder()
                .status(SUCCESS)
                .results(deviceMonitorResponseDTO)
                .build();

        log.info("DeviceController::updateInfoDevice response {}", deviceMonitorResponseDTO );
        return new ResponseEntity<>(responseDTO, HttpStatus.ACCEPTED);
    }
    @GetMapping("getInfoDeviceById")
    public ResponseEntity<APIResponse> getInfoDeviceByName(@RequestParam String DeviceName) {
        log.info("DeviceController::getInfoDeviceById by idDevice  {}", DeviceName);
        DeviceMonitorResponseDTO deviceResponseDTO = deviceServiceImp.getInfoDeviceByName(DeviceName);
        APIResponse<DeviceMonitorResponseDTO> responseDTO = APIResponse
                .<DeviceMonitorResponseDTO>builder()
                .status(SUCCESS)
                .results(deviceResponseDTO)
                .build();

        log.info("DeviceController::getInfoDeviceById by idDevice {} response {}", DeviceName,deviceResponseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @PostMapping("robot-test")
    public ResponseEntity<APIResponse> getDevicesRobotTest(@Valid @RequestBody DeviceSiteRequest deviceSiteRequest) {
        log.info("DeviceController::getDevicesRobotTest Request Param {}", deviceSiteRequest);
        Collection<DeviceResponseRobotL3vpn> devices = deviceServiceImp.getDevicesRobotTest(deviceSiteRequest);
        APIResponse<List<DeviceResponseRobotL3vpn>> responseDTO = APIResponse
                .<List<DeviceResponseRobotL3vpn>>builder()
                .status(SUCCESS)
                .results((List<DeviceResponseRobotL3vpn>) devices)
                .build();

        log.info("DeviceController::getDevicesRobotTestL3VPN response {}", devices);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @PostMapping("getRouters")
    public Map<String, RouterDTO> getRouters(@Valid @RequestBody DeviceSiteRequest deviceSiteRequest) {
        log.info("DeviceController::getRouters Request Param {}", deviceSiteRequest);
        Map<String, RouterDTO> routers = deviceServiceImp.getRouters(deviceSiteRequest);

        log.info("DeviceController::getRouters response {}", routers);

        return routers;
    }

    @PostMapping("getRouterCE")
    public Map<String, RouterDTO> getRoutersCE(@Valid @RequestBody DeviceSiteRequest deviceSiteRequest) {
        log.info("DeviceController::getRoutersCE Request Param {}", deviceSiteRequest);
        Map<String, RouterDTO> routers = deviceServiceImp.getRoutersCE(deviceSiteRequest);

        log.info("DeviceController::getRoutersCE response {}", routers);

        return routers;
    }
}
