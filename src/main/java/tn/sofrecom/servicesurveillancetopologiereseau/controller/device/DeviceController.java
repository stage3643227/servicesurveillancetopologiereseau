package tn.sofrecom.servicesurveillancetopologiereseau.controller.device;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.APIResponse;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.device.DeviceRequestDTO;

public interface DeviceController {
    @PostMapping
    ResponseEntity<APIResponse> createNewDevice(DeviceRequestDTO deviceRequestDTO);
    @PutMapping
    ResponseEntity<APIResponse> updateDevice(DeviceRequestDTO deviceRequestDTO);
    @DeleteMapping("/{idDevice}")
    ResponseEntity<Void> deleteDevice(@PathVariable Long idDevice);
    @GetMapping("/{idDevice}")
    ResponseEntity<APIResponse> getDeviceById(@PathVariable Long idDevice);
    @GetMapping("/all")
    ResponseEntity<APIResponse> getAllDevices();
}
