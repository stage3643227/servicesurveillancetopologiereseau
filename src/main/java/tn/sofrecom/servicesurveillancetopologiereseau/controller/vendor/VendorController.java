package tn.sofrecom.servicesurveillancetopologiereseau.controller.vendor;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.APIResponse;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorRequestDTO;

public interface VendorController {
    @PostMapping
    ResponseEntity<APIResponse> createNewVendor(VendorRequestDTO vendorRequestDTO);
    @PutMapping
    ResponseEntity<APIResponse> updateVendor(VendorRequestDTO vendorRequestDTO);
    @DeleteMapping("/{idVendor}")
    ResponseEntity<Void> deleteVendor(@PathVariable Long idVendor);
    @GetMapping("/{idVendor}")
    ResponseEntity<APIResponse> getVendorById(@PathVariable Long idVendor);
    @GetMapping("/all")
    ResponseEntity<APIResponse> getAllVendors();
}
