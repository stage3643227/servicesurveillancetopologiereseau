package tn.sofrecom.servicesurveillancetopologiereseau.controller.vendor.implement;

import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tn.sofrecom.servicesurveillancetopologiereseau.controller.vendor.VendorController;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.APIResponse;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorRequestDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.dto.vendor.VendorResponseDTO;
import tn.sofrecom.servicesurveillancetopologiereseau.service.vendor.implement.VendorServiceImp;

import java.util.Collection;
import java.util.List;

@RestController
@AllArgsConstructor
@Slf4j
@RequestMapping("/api/v1/Vendor")
@CrossOrigin(origins = "http://localhost:4200")
public class VendorControllerImp implements VendorController {
    public static final String SUCCESS = "Success";
    private VendorServiceImp vendorServiceImp;
    @Override
    public ResponseEntity<APIResponse> createNewVendor(@Valid @RequestBody VendorRequestDTO vendorRequestDTO) {
        log.info("VendorController::createNewVendor request body {}", vendorRequestDTO);

        VendorResponseDTO vendorResponseDTO = vendorServiceImp.createNewVendor(vendorRequestDTO);
        APIResponse<VendorResponseDTO> responseDTO = APIResponse
                .<VendorResponseDTO>builder()
                .status(SUCCESS)
                .results(vendorResponseDTO)
                .build();

        log.info("VendorController::createNewVendor  response {}", vendorResponseDTO);
        return new ResponseEntity<>(responseDTO, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<APIResponse> updateVendor(@Valid @RequestBody VendorRequestDTO vendorRequestDTO) {
        log.info("VendorController::updateVendor request body {}", vendorRequestDTO);
        VendorResponseDTO vendorResponseDTO  = vendorServiceImp.updateVendor(vendorRequestDTO);
        APIResponse<VendorResponseDTO> responseDTO = APIResponse
                .<VendorResponseDTO>builder()
                .status(SUCCESS)
                .results(vendorResponseDTO)
                .build();

        log.info("VendorController::updateVendor response {}", vendorResponseDTO);
        return new ResponseEntity<>(responseDTO, HttpStatus.ACCEPTED);
    }

    @Override
    public ResponseEntity<Void> deleteVendor(Long idVendor) {
        log.info("VendorController::deleteVendor by idVendor  {}", idVendor);
        vendorServiceImp.deleteVendor(idVendor);
        return ResponseEntity.ok().build();
    }

    @Override
    public ResponseEntity<APIResponse> getVendorById(Long idVendor) {
        log.info("VendorController::getVendorById by idVendor  {}", idVendor);

        VendorResponseDTO vendorResponseDTO  = vendorServiceImp.getVendorById(idVendor);
        APIResponse<VendorResponseDTO> responseDTO = APIResponse
                .<VendorResponseDTO>builder()
                .status(SUCCESS)
                .results(vendorResponseDTO)
                .build();

        log.info("VendorController::getVendorById by idVendor {} response {}", idVendor, vendorResponseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<APIResponse> getAllVendors() {
        Collection<VendorResponseDTO> vendorResponseDTOS = vendorServiceImp.getVendors();
        APIResponse<List<VendorResponseDTO>> responseDTO = APIResponse
                .<List<VendorResponseDTO>>builder()
                .status(SUCCESS)
                .results((List<VendorResponseDTO>) vendorResponseDTOS)
                .build();

        log.info("VendorController::getAllVendors response {}", responseDTO);

        return new ResponseEntity<>(responseDTO, HttpStatus.OK);
    }
}
