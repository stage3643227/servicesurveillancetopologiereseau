package tn.sofrecom.servicesurveillancetopologiereseau.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "VENDOR")
public class Vendor implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_vendor")
    private Long idVendor;
    @Column(name = "vendor_name")
    private String vendorName;
    @Column(name = "driver")
    private String driver;

    @OneToMany(mappedBy ="vendor", cascade = CascadeType.REMOVE)
    @JsonBackReference
    private Collection<Device> devices;
}
