package tn.sofrecom.servicesurveillancetopologiereseau.model;

import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "NETINTERFACE")
public class NetInterface implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_net_interface")
    private Long idInterface;
    @Column(name = "name_interface")
    private String nameInterface;

    @Column(name = "ip_address_interface")
    private String ipAddressInterface;

    @Column(name = "interface_state")
    private String interfaceState;

    @Transient
    private Long idDevice;

    @ManyToOne
    @JoinColumn(name = "id_device")
    @ToString.Exclude
    private Device device;
}
