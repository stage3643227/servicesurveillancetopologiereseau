package tn.sofrecom.servicesurveillancetopologiereseau.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;

import java.io.Serializable;
import java.util.Collection;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Entity
@Table(name = "DEVICE")
public class Device  implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_device")
    private Long idDevice;
    @Column(name = "device_name")
    private String deviceName;
    @Column(name = "device_type")
    private String deviceType;
    @Column(name = "cpu_value")
    private Integer cpuValue;
    @Column(name = "memory_value")
    private Integer memoryValue;
    @Column(name = "ip_address")
    private String ipAddress;
    @Column(name = "port")
    private String port;
    @Column(name = "transport")
    private String transport;

    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;

    @Column(name = "id_site")
    private Long idSite;

    @Transient
    private Long idVendor;
    @OneToMany(mappedBy ="device", cascade = CascadeType.REMOVE)
    @JsonBackReference
    private Collection<NetInterface> netInterfaces;
    @ManyToOne
    @JoinColumn(name = "id_vendor")
    @ToString.Exclude
    private Vendor vendor;




}
