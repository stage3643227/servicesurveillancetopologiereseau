package tn.sofrecom.servicesurveillancetopologiereseau.subject;

import tn.sofrecom.servicesurveillancetopologiereseau.model.NetInterface;
import tn.sofrecom.servicesurveillancetopologiereseau.observer.StateObserver;

public interface SubjectNetInterface {
    void registerObserver(StateObserver observer);
    void removeObserver(StateObserver observer);
    void notifyObservers(NetInterface netInterface);
}
