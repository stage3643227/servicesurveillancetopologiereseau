package tn.sofrecom.servicesurveillancetopologiereseau.subject;

import tn.sofrecom.servicesurveillancetopologiereseau.model.Device;
import tn.sofrecom.servicesurveillancetopologiereseau.observer.StateObserver;

public interface SubjectDevice {
    void registerObserver(StateObserver observer);
    void removeObserver(StateObserver observer);
    void notifyObservers(Device device);
}
