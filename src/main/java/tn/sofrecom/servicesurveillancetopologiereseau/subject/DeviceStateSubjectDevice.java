package tn.sofrecom.servicesurveillancetopologiereseau.subject;

import org.springframework.stereotype.Component;
import tn.sofrecom.servicesurveillancetopologiereseau.model.Device;
import tn.sofrecom.servicesurveillancetopologiereseau.observer.StateObserver;

import java.util.List;

@Component
public class DeviceStateSubjectDevice implements SubjectDevice {
    private List<StateObserver> observers;

    public DeviceStateSubjectDevice(List<StateObserver> observers) {
        this.observers = observers;
    }

    @Override
    public void registerObserver(StateObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(StateObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(Device device) {
        for (StateObserver observer : observers) {
            observer.update(device);
        }
    }
}
