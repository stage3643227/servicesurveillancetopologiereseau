package tn.sofrecom.servicesurveillancetopologiereseau.subject;
import org.springframework.stereotype.Component;
import tn.sofrecom.servicesurveillancetopologiereseau.model.NetInterface;
import tn.sofrecom.servicesurveillancetopologiereseau.observer.StateObserver;

import java.util.List;
@Component
public class NetInterfaceStateSubject implements SubjectNetInterface {
    private List<StateObserver> observers;

    public NetInterfaceStateSubject(List<StateObserver> observers) {
        this.observers = observers;
    }

    @Override
    public void registerObserver(StateObserver observer) {
        observers.add(observer);
    }

    @Override
    public void removeObserver(StateObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers(NetInterface netInterface) {
        for (StateObserver observer : observers) {
            observer.update(netInterface);
        }
    }
}
