---29/03/2023--
create table public.device
(
    id_device    bigint not null
        primary key,
    cpu_value    integer,
    device_name  varchar(255),
    id_site      bigint,
    memory_value integer,
    id_vendor    bigint
        constraint fkraytij7h3cwtgibseiejt8nj6
            references public.vendor,
    device_type  varchar(255)
);

alter table public.device
    owner to postgres;

create table public.netinterface
(
    id_net_interface bigint not null
        primary key,
    interface_state  varchar(255),
    name_interface   varchar(255),
    id_device        bigint
        constraint fkkxpndsdpakmefcv4gb11bct67
            references public.device
);

alter table public.netinterface
    owner to postgres;

create table public.vendor
(
    id_vendor   bigint not null
        primary key,
    vendor_name varchar(255)
);

alter table public.vendor
    owner to postgres;

